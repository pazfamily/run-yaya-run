# Run, Bucket, Run

Run, Bucket, Run is a Bitbucket FileView add-on that renders your code as a platform game.

For a live demo and installation instructions, head to https://run-bucket-run.aerobatic.io/

To learn more about how it was developed, see https://developer.atlassian.com/blog/2015/10/bitbucket-fileview-addon/